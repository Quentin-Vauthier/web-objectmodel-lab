const { Humidity } = require('./src/sensors.js');
const { importFromObj } = require('./src/index.js');
const sensors = [];
const h = new Humidity(48569, 'Humidité SDB');
sensors.push(h);
const d = new Date();
d.setHours(6);
d.setMinutes(0);

for(let i = 1; i <= 5; ++i) {
    h.register(Math.floor(Math.random() * 4 + 38), d.toISOString());
    d.setMinutes(i*10);
}
d.setMinutes(50);
h.register(41, d.toISOString());
d.setMinutes(0);
d.setHours(7);
h.register(40, d.toISOString());
d.setMinutes(10);
h.register(42, d.toISOString());
d.setMinutes(20);
h.register(50, d.toISOString());
d.setMinutes(30);
h.register(65, d.toISOString());
d.setMinutes(40);
h.register(60, d.toISOString());
d.setMinutes(50);
h.register(53, d.toISOString());
d.setMinutes(0);
d.setHours(8)
h.register(45, d.toISOString());
d.setMinutes(10);
h.register(40, d.toISOString());
d.setMinutes(20);
h.register(39, d.toISOString());
for(let i = 3; i <= 5; ++i) {
    d.setMinutes(i*10);
    h.register(Math.floor(Math.random() * 4 + 38), d.toISOString());
}
d.setMinutes(0);
d.setHours(9);
h.register(38, d.toISOString());

const temp = importFromObj({
    "id": 1234,
    "name": "Température Bureau",
    "type": "TEMPERATURE",
    "data": {
        "values": [23, 23, 22, 21, 23, 23, 23, 25, 25],
        "labels": [
            "2021-01-19T08:00:00.000Z",
            "2021-01-19T09:00:00.000Z",
            "2021-01-19T10:00:00.000Z",
            "2021-01-19T11:00:00.000Z",
            "2021-01-19T12:00:00.000Z",
            "2021-01-19T13:00:00.000Z",
            "2021-01-19T14:00:00.000Z",
            "2021-01-19T15:00:00.000Z",
            "2021-01-19T16:00:00.000Z"
        ]
    }
});
sensors.push(temp);

const porteGarage = importFromObj({
    "id": 10245,
    "name": "Porte du Garage",
    "type": "DOOR",
    "data": {
        "value": 0
    }
});
sensors.push(porteGarage);

const ventilateur = importFromObj({
    "id": 2222,
    "name": "Ventilateur Ordinateur Bureau",
    "type": "FAN_SPEED",
    "data": {
        "values": [1073, 1800, 2299, 2176, 1899, 1400],
        "labels": [
        "2021-01-19T10:00:00.000Z",
        "2021-01-19T10:05:00.000Z",
        "2021-01-19T10:10:00.000Z",
        "2021-01-19T10:15:00.000Z",
        "2021-01-19T10:20:00.000Z",
        "2021-01-19T10:25:00.000Z"
        ]
    }
});
sensors.push(ventilateur);


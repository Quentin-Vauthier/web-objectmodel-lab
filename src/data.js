class Data {}
class TimeSeries extends Data {
  #values;
  #labels;
  constructor(values, labels) {
    super();
    if (values && !Array.isArray(values))
      throw new TypeError('values must be an array');
    if (labels && !Array.isArray(labels))
      throw new TypeError('labels must be an array');
    if (labels && values && labels.length !== values.length)
      throw new TypeError('Labels and values must have the same length');
    if (labels?.some((l) => typeof l !== 'string'))
      throw new TypeError('Labels must all be strings');
    if (values?.some((v) => typeof v !== 'number'))
      throw new TypeError('Values must all be numbers');
    this.#values = values || [];
    this.#labels = labels || [];
  }
  register(value, label) {
    if (typeof value !== 'number')
      throw new TypeError('value must be a number');
    if (typeof label !== 'string')
      throw new TypeError('labels must be a string');
    this.#values.push(value);
    this.#labels.push(label);
  }
  average(lastValuesCnt) {
    let averageFrom = this.#values;
    if (lastValuesCnt)
      averageFrom = averageFrom.slice(averageFrom.length - lastValuesCnt);
    return averageFrom.reduce((acc, val) => acc + val) / averageFrom.length;
  }
  lastValues(count) {
    return this.#values.slice(this.#values.length - count);
  }
  getValues() {
    return this.#values.slice();
  }
  getLabels() {
    return this.#labels.slice();
  }
  toJson() {
    return {
      labels: this.#labels,
      values: this.#values
    };
  }
}
class Datum extends Data {
  #value;
  constructor(value) {
    super();
    if (value !== null && value !== undefined && typeof value !== 'number') {
      console.log(`Fucked up value : ${value}`);
      throw new TypeError('value must be a number');
    }
    this.#value = value ?? null;
  }
  register(value) {
    if (typeof value !== 'number')
      throw new TypeError('value must be a number');
    this.#value = value;
  }
  getValue() {
    return this.#value;
  }
  toJson() {
    return {
      value: this.#value
    };
  }
}
class PercentageSeries extends TimeSeries {
  constructor(values, labels) {
    if (values && values.some((v) => v < 0 || v > 100))
      throw new TypeError(
        `The values must all be between 1 and 100, these ones does not match this condition : (${values
          .filter((v) => v < 0 || v > 100)
          .join(', ')})`
      );
    super(values, labels);
  }
  register(value, label) {
    if (value > 100 || value < 0)
      throw new TypeError('value must be between 0 and 100');
    super.register(value, label);
  }
}
class TinyInt extends Datum {
  constructor(value) {
    super(value ?? 0);
    if (value && value !== 0 && value !== 1)
      throw new TypeError(`The value must be 0 or 1`);
  }
  register(value) {
    if (value !== 0 && value !== 1)
      throw new TypeError(`The value must be 0 or 1`);
    super.register(value);
  }
}
class Percentage extends Datum {
  constructor(value) {
    super(value || 0);
    if (value < 0 || value > 100)
      throw new TypeError(`The value must be between 0 and 100`);
  }
  register(value) {
    if (value < 0 || value > 100)
      throw new TypeError(`The value must be between 0 and 100`);
    super.register(value);
  }
}
module.exports = {
  Data: Data,
  Datum: Datum,
  TimeSeries: TimeSeries,
  PercentageSeries: PercentageSeries,
  TinyInt: TinyInt,
  Percentage: Percentage,
};

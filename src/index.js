const version = () => '1.0.0';
const fs = require('fs');
const { sensorTypeToConstructor } = require('./sensors.js');
async function importFromFile(file) {
  let data;
  data = fs.readFileSync(file, {
    encoding: 'utf8',
  });
  return importFromObj(JSON.parse(data));
}

async function importFromObj(data) {
  let createdSensors = [];
  data.forEach(sensor => {
    let { type } = sensor;
    for(let sensorType in sensorTypeToConstructor) {
      if(type.toUpperCase() === sensorType) {
        createdSensors.push(sensorTypeToConstructor[sensorType].fromJson(sensor));
      }
    }
  });
  return createdSensors;
}

module.exports = {
  version: version,
  importFromFile: importFromFile,
  importFromObj: importFromObj
};
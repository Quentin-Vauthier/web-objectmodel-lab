const {
  TimeSeries,
  Datum,
  PercentageSeries,
  TinyInt,
  Percentage,
} = require('./data.js');

class Sensor {
  #id;
  #name;
  #type;
  #data;
  static sensorTypes = [
    'TEMPERATURE',
    'HUMIDITY',
    'LIGHT',
    'SWITCH',
    'DOOR',
    'FAN_SPEED',
  ];
  static fromJson(obj) {
    return new Sensor(obj.id, obj.name, obj.type, obj.data);
  }
  constructor(id, name, type, data) {
    if (!Sensor.sensorTypes.includes(type))
      throw new TypeError(
        `type must be one of sensorTypes (${Sensor.sensorTypes.join(', ')})`
      );
    this.#id = id;
    this.#name = name;
    this.#type = type;
    this.#data = data || new Datum();
  }
  getId() {
    return this.#id;
  }
  getName() {
    return this.#name;
  }
  getType() {
    return this.#type;
  }
  getData() {
    return this.#data;
  }
  register(p1, p2) {
    this.#data.register(p1, p2);
  }
  toJson() {
    return {
      id: this.#id,
      name: this.#name,
      type: this.#type,
      data: this.#data.toJson(),
    };
  }
}

class Temperature extends Sensor {
  #unit;
  static fromJson(obj) {
    let ts = new TimeSeries(obj.data.values, obj.data.labels);
    return new Temperature(obj.id, obj.name, obj.unit, ts);
  }
  constructor(id, name, unit, data) {
    super(id, name, 'TEMPERATURE', data || new TimeSeries());
    this.#unit = unit || 'C';
  }
  getUnit() {
    return this.#unit;
  }
  toCelcius() {
    const data = this.getData();
    let values = data.getValues();
    let labels = data.getLabels();
    switch (this.#unit) {
      case 'C':
        return new TimeSeries(values, labels);
      case 'K':
        return new TimeSeries(
          values.map((e) => e - 273.15),
          labels
        );
      case 'F':
        return new TimeSeries(
          values.map((e) => (e - 32) * (5 / 9)),
          labels
        );
      default:
        throw new TypeError(
          `Conversion from unit ${this.#unit} hasn't been implemented`
        );
    }
  }
  toFarenheit() {
    const data = this.getData();
    let values = data.getValues();
    let labels = data.getLabels();
    switch (this.#unit) {
      case 'C':
        return new TimeSeries(
          values.map((e) => e * (9 / 5) + 32),
          labels
        );
      case 'K':
        return new TimeSeries(
          values.map((e) => (e - 273.15) * (9 / 5) + 32),
          labels
        );
      case 'F':
        return new TimeSeries(values, labels);
      default:
        throw new TypeError(
          `Conversion from unit ${this.#unit} hasn't been implemented`
        );
    }
  }
  toKelvin() {
    const data = this.getData();
    let values = data.getValues();
    let labels = data.getLabels();
    switch (this.#unit) {
      case 'C':
        return new TimeSeries(
          values.map((e) => e + 273.15),
          labels
        );
      case 'K':
        return new TimeSeries(values, labels);
      case 'F':
        return new TimeSeries(
          values.map((e) => (e - 32) * (5 / 9) + 273.15),
          labels
        );
      default:
        throw new TypeError(
          `Conversion from unit ${this.#unit} hasn't been implemented`
        );
    }
  }
}

class Humidity extends Sensor {
  static fromJson(obj) {
    let p = new PercentageSeries(obj.data.values, obj.data.labels);
    return new Humidity(obj.id, obj.name, p);
  }
  constructor(id, name, data) {
    super(id, name, 'HUMIDITY', data || new PercentageSeries());
    if (data && data.constructor.name !== 'PercentageSeries')
      throw new TypeError(`The data for this sensor must be PercentageSeries`);
  }
}

class Light extends Sensor {
  static fromJson(obj) {
    return new Light(obj.id, obj.name, obj.data.value);
  }
  constructor(id, name, value) {
    super(id, name, 'LIGHT', new Percentage(value ?? 0));
  }
  isOn() {
    return this.getData().getValue() > 0;
  }
  isBright() {
    return this.getData().getValue() > 80;
  }
}

class Switch extends Sensor {
  static fromJson(obj) {
    return new Switch(obj.id, obj.name, obj.data.value);
  }
  constructor(id, name, value) {
    super(id, name, 'SWITCH', new TinyInt(value ?? 0));
  }
  isOn() {
    return this.getData().getValue() === 1;
  }
}

class Door extends Sensor {
  static fromJson(obj) {
    return new Door(obj.id, obj.name, obj.data.value);
  }
  constructor(id, name, value) {
    super(id, name, 'DOOR', new TinyInt(value ?? 0));
  }
  isOpened() {
    return this.getData().getValue() === 0;
  }
}

class FanSpeed extends Sensor {
  static fromJson(obj) {
    let ts = new TimeSeries(obj.data.values, obj.data.labels);
    return new FanSpeed(obj.id, obj.name, ts);
  }
  constructor(id, name, data) {
    if (data && data.constructor.name !== 'TimeSeries')
      throw new TypeError(
        `The data for this sensor must be of type TimeSeries`
      );
    super(id, name, 'FAN_SPEED', data ?? new TimeSeries());
  }
  isCurrentlyOver(rpm) {
    return [this.getData().lastValues(1)] > rpm;
  }
}

const sensorTypeToConstructor = {
  TEMPERATURE: Temperature,
  HUMIDITY: Humidity,
  LIGHT: Light,
  SWITCH: Switch,
  DOOR: Door,
  FAN_SPEED: FanSpeed,
};
/*
const sensorTypeToDataType = {
    'TEMPERATURE': TimeSeries,
    'HUMIDITY': PercentageSeries,
    'LIGHT': Percentage,
    'SWITCH': TinyInt,
    'DOOR': TinyInt,
    'FAN_SPEED': TimeSeries
};
*/

module.exports = {
  Sensor: Sensor,
  Temperature: Temperature,
  Humidity: Humidity,
  Light: Light,
  Switch: Switch,
  Door: Door,
  FanSpeed: FanSpeed,
  //sensorTypeToDataType: sensorTypeToDataType,
  sensorTypeToConstructor: sensorTypeToConstructor,
};

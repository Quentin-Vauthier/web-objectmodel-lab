const fs = require('fs').promises;

import { version } from '.';
import { importFromObj, importFromFile } from '.';

let data;
beforeAll(async () => {
  data = await fs.readFile('./resources/sensors_data.json', {
    encoding: 'utf8',
  });
  data = JSON.parse(data);
});

describe('Sensor model tests', () => {
  describe('Dummy tests', () => {
    test('data is loaded with 3 elements', () => {
      expect(data.length).toBe(3);
    });
    test('version number from the model', () => {
      expect(version()).toBe('1.0.0');
    });
  });
  describe('Import', () => {
    test('import data', async () => {
      let created = await importFromObj(data);
      let temp = created.find(e => e.getId() === 1234);
      expect(temp.getData().getValues()).toStrictEqual([23, 23, 22, 21, 23, 23, 23, 25, 25]);
    });
  });
  describe('Import from file', () => {
    test('import file', async () => {
      let created = await importFromFile('./resources/sensors_data.json');
      let temp = created.find(e => e.getId() === 1234);
      expect(temp.getData().getValues()).toStrictEqual([23, 23, 22, 21, 23, 23, 23, 25, 25]);
    });
  });
});

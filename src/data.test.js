const {
  TimeSeries,
  Datum,
  PercentageSeries,
  TinyInt,
  Percentage,
} = require('./data.js');

describe('Data tests', () => {
  describe('Timeseries tests', () => {
    let emptyTs = new TimeSeries();
    let filledTs = new TimeSeries(
      [1, 2, 3, 4, 5, 6, 7, 8, 9],
      ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    );
    test('Get values', () => {
      expect(filledTs.getValues()).toStrictEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    test('Get labels', () => {
      expect(filledTs.getLabels()).toStrictEqual(['1', '2', '3', '4', '5', '6', '7', '8', '9',]);
    });
    test('Test register', () => {
      emptyTs.register(1, 'First value');
      expect(emptyTs.getLabels()[0]).toBe('First value');
      expect(emptyTs.getValues()[0]).toBe(1);
    });
    test('Test average', () => {
      expect(filledTs.average()).toBe(5);
    });
    test('Average of the last 2 values', () => {
      expect(filledTs.average(2)).toBe(8.5);
    });
    test('Creating incorrect objects', () => {
      expect(() => new TimeSeries(1, ['1', '2', '3'])).toThrow(TypeError);
      expect(() => new TimeSeries([1, 2, 3], '1,2,3')).toThrow(TypeError);
      expect(() => new TimeSeries([1, 2, 3], ['1', '2', '3', '4'])).toThrow(
        TypeError
      );
    });
    test('Other invalid constructors', () => {
      expect(() => new TimeSeries(['Not an int'], ['label'])).toThrow(
        TypeError
      );
      expect(() => new TimeSeries([1, 2], [1, 2])).toThrow(TypeError);
    });
    test('Registerig incorrect values', () => {
      expect(() => emptyTs.register('1', '1')).toThrow(TypeError);
      expect(() => emptyTs.register(1, 1)).toThrow(TypeError);
    });
    test('Returning last values', () => {
      expect(filledTs.lastValues(2)).toStrictEqual([8, 9]);
    });
    test('Using lastValues without parameter', () => {
      expect(filledTs.lastValues()).toStrictEqual(filledTs.getValues());
    });
  });
  describe('Datum tests', () => {
    let datum = new Datum(1);
    test('Constructeurs', () => {
      let d = new Datum();
      expect(d.getValue()).toBeNull();
      let d2 = new Datum(0);
      expect(d2.getValue()).toBe(0);
    });
    test('Get value', () => {
      expect(datum.getValue()).toBe(1);
    });
    test('Register', () => {
      datum.register(0);
      expect(datum.getValue()).toBe(0);
    });
    test('Register wrong value', () => {
      expect(() => datum.register('Invalid')).toThrow(TypeError);
    });
    test('Invalid constructor', () => {
      expect(() => new Datum('Invalid')).toThrow(TypeError);
    });
  });
  describe('PercentageSeries tests', () => {
    let p = new PercentageSeries();
    test('Invalid constructors', () => {
      expect(() => new PercentageSeries([-1], ['-1'])).toThrow(TypeError);
      expect(() => new PercentageSeries([101], ['101'])).toThrow(TypeError);
    });
    test('Register invalid', () => {
      expect(() => p.register(-1, 'Test')).toThrow(TypeError);
      expect(() => p.register(101, 'Test')).toThrow(TypeError);
    });
    test('Register valid', () => {
      p.register(1, 'Test');
      expect(p.getValues()).toStrictEqual([1]);
    });
  });
  describe('TinyInt and Percentage', () => {
    test('TinyInt constructor', () => {
      expect(() => new TinyInt(2)).toThrow(TypeError);
      expect(new TinyInt().getValue()).toBe(0);
      expect(new TinyInt(1).getValue()).toBe(1);
    });
    test('TinyInt register invalid', () => {
      let ti = new TinyInt(1);
      expect(() => ti.register(2)).toThrow(TypeError);
      ti.register(0);
      expect(ti.getValue()).toBe(0);
    });
    test('Percentage constructor', () => {
      expect(() => new Percentage(-1)).toThrow(TypeError);
      expect(() => new Percentage(101)).toThrow(TypeError);
      expect(new Percentage().getValue()).toBe(0);
      expect(new Percentage(100).getValue()).toBe(100);
    });
    test('Percentage register invalid', () => {
      let p = new Percentage(0);
      expect(() => p.register(101)).toThrow(TypeError);
      p.register(100);
      expect(p.getValue()).toBe(100);
    });
  });
});

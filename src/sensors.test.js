const {
  Sensor,
  Temperature,
  Humidity,
  Light,
  Switch,
  Door,
  FanSpeed,
} = require('./sensors.js');
const { Datum, TimeSeries, Percentage } = require('./data.js');

describe('Sensors tests', () => {
  describe('Basic tests', () => {
    test('Invalid type', () => {
      expect(() => new Sensor(1, 'name', 'NOT_A_TYPE')).toThrow(TypeError);
    });
    test('Instantiate with data', () => {
      let s = new Sensor(1, 'name', 'TEMPERATURE', new Datum(1));
      expect(s.getData().getValue()).toBe(1);
    });
    test('Instantiate without data', () => {
      let s = new Sensor(1, 'name', 'TEMPERATURE');
      expect(s.getData().getValue()).toBeNull();
    });
    test('Getters', () => {
      let s = new Sensor(1, 'name', 'TEMPERATURE');
      expect(s.getId()).toBe(1);
      expect(s.getName()).toBe('name');
    });
    test('Import JSON', () => {
      let s = Sensor.fromJson({
        id: 1234,
        name: 'Température Bureau',
        type: 'TEMPERATURE',
        data: {
          values: [23, 23, 22, 21, 23, 23, 23, 25, 25],
          labels: [
            '2021-01-19T08:00:00.000Z',
            '2021-01-19T09:00:00.000Z',
            '2021-01-19T10:00:00.000Z',
            '2021-01-19T11:00:00.000Z',
            '2021-01-19T12:00:00.000Z',
            '2021-01-19T13:00:00.000Z',
            '2021-01-19T14:00:00.000Z',
            '2021-01-19T15:00:00.000Z',
            '2021-01-19T16:00:00.000Z',
          ],
        },
      });
      expect(s.getId()).toBe(1234);
      expect(s.getName()).toBe('Température Bureau');
      expect(s.getType()).toBe('TEMPERATURE');
    });
  });
  describe('Temperature sensor', () => {
    let valuesC = new TimeSeries([0, 20], ['0', '20']);
    let valuesK = new TimeSeries([273.15], ['273,15']);
    let valuesF = new TimeSeries([5, 50], ['5', '50']);
    let celsius = new Temperature(1, 'celsius', 'C', valuesC);
    let kelvin = new Temperature(1, 'kelvin', 'K', valuesK);
    let farenheit = new Temperature(1, 'farenheit', 'F', valuesF);
    test('Constructor without unit', () => {
      let t = new Temperature(1, 't');
      expect(t.getUnit()).toBe('C');
    });
    test('Tests de conversion depuis celsius', () => {
      expect(celsius.toFarenheit().getValues()).toStrictEqual([32, 68]);
      expect(celsius.toKelvin().getValues()).toStrictEqual([273.15, 293.15]);
      expect(celsius.toCelcius().getValues()).toStrictEqual(
        celsius.getData().getValues()
      );
    });
    test('Tests de conversion depuis farenheit', () => {
      expect(farenheit.toCelcius().getValues()).toStrictEqual([-15, 10]);
      expect(farenheit.toKelvin().getValues()).toStrictEqual([258.15, 283.15]);
      expect(farenheit.toFarenheit().getValues()).toStrictEqual(
        farenheit.getData().getValues()
      );
    });
    test('Tests de conversion depuis kelvin', () => {
      expect(kelvin.toFarenheit().getValues()).toStrictEqual([32]);
      expect(kelvin.toCelcius().getValues()).toStrictEqual([0]);
      expect(kelvin.toKelvin().getValues()).toStrictEqual(
        kelvin.getData().getValues()
      );
    });
    test('Conversion invalide', () => {
      let t = new Temperature(1, 't', 'NOT_A_UNIT');
      expect(() => t.toCelcius()).toThrow(TypeError);
      expect(() => t.toFarenheit()).toThrow(TypeError);
      expect(() => t.toKelvin()).toThrow(TypeError);
    });
  });
  describe('Humidity sensor', () => {
    test('Constructors', () => {
      expect(() => new Humidity(1, 'h', new TimeSeries())).toThrow(TypeError);
      let h = new Humidity(1, 'h');
      h.register(50, 'Test');
      expect(h.getData().getValues()).toStrictEqual([50]);
    });
    test('import', () => {
      let h = Humidity.fromJson({
        id: 1,
        name: 'Humidité salle',
        type: 'HUMIDITY',
        data: {
          values: [1, 2, 3, 4, 5],
          labels: ['1', '2', '3', '4', '5'],
        },
      });
      expect(h.getId()).toBe(1);
      expect(h.getName()).toBe('Humidité salle');
      expect(h.getType()).toBe('HUMIDITY');
    });
  });
  describe('Light', () => {
    test('Constructor', () => {
      expect(() => new Light(1, 'l', -1)).toThrow(TypeError);
      expect(new Light(1, 'l', 50).getData().getValue()).toBe(50);
    });
    test('Custom methods', () => {
      let lOff = new Light(0, 'off');
      let l = new Light(1, 'l', 60);
      let lBright = new Light(2, 'l2', 90);
      expect(lOff.isOn()).toBeFalsy();
      expect(l.isOn()).toBeTruthy();
      expect(lBright.isBright()).toBeTruthy();
    });
    test('import', () => {
      let h = Light.fromJson({
        id: 0,
        name: 'Lumiere Salon',
        type: 'LIGHT',
        data: {
          value: 0,
        },
      });
      expect(h.getId()).toBe(0);
      expect(h.getName()).toBe('Lumiere Salon');
      expect(h.getType()).toBe('LIGHT');
    });
  });
  describe('Switch', () => {
    test('Constructor', () => {
      expect(() => new Switch(1, 's', -1)).toThrow(TypeError);
      expect(new Switch(1, 's', 1).getData().getValue()).toBe(1);
      expect(new Switch(1, 's').getData().getValue()).toBe(0);
    });
    test('Custom methods', () => {
      let s = new Switch(1, 's', 1);
      expect(s.isOn()).toBeTruthy();
    });
  });
  describe('Door', () => {
    test('Constructor', () => {
      expect(() => new Door(1, 'd', -1)).toThrow(TypeError);
      expect(new Door(2, 'd2', 1).getData().getValue()).toBe(1);
      expect(new Door(3, 'd3').getData().getValue()).toBe(0);
    });
    test('Custom methods', () => {
      let d = new Door(1, 'd');
      expect(d.isOpened()).toBeTruthy();
      d.register(1);
      expect(d.isOpened()).toBeFalsy();
    });
    test('import', () => {
      let h = Switch.fromJson({
        id: 0,
        name: 'Interrupteur',
        type: 'SWITCH',
        data: {
          value: 0,
        },
      });
      expect(h.getId()).toBe(0);
      expect(h.getName()).toBe('Interrupteur');
      expect(h.getType()).toBe('SWITCH');
    });
  });
  describe('FanSpeed', () => {
    test('Constructor', () => {
      expect(() => new FanSpeed(1, 'f', new Percentage(0))).toThrow(TypeError);
    });
    test('Custom methods', () => {
      let f = new FanSpeed(1, 'f');
      f.register(2400, 'Now');
      expect(f.isCurrentlyOver(2000)).toBeTruthy();
    });
  });
});
